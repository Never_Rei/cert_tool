# cert_tool
SSL Certificate Checking Tool that checks a list of defined HTTPS services and notifies before expiry.

## Notifications
Currently this tool can only notify via Slack Web Hooks but there are plans to add other services like pager duty ETC.

### Configuring Notifications
