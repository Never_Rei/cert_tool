CREATE TABLE certs (
    cert_id INTEGER NOT NULL PRIMARY KEY,
    cert_hostname TEXT NOT NULL,
    cert_serial TEXT NOT NULL,
    cert_first_seen TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    cert_last_checked TIMESTAMP,
    cert_expires_in INTEGER NOT NULL
);

INSERT INTO certs (
    cert_hostname,
    cert_serial,
    cert_expires_in
) values (
"test.com",
"123551412311",
"756"
);