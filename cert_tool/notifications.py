import logging
import json
import requests
from abc import ABC, abstractmethod
from typing import List

from slack_sdk.webhook import WebhookClient
import config
from slack_blocks import SlackTextBlock, SlackSectionBlock, SlackHeaderBlock, SlackBlockKit


class Notification(ABC):
    """Represents a basic notification and serves as the base class for notifications."""
    NOTIFICATION_TYPES = ['CRITICAL', 'WARNING', 'INFO']

    def __init__(self, notification_type):
        if notification_type in self.NOTIFICATION_TYPES:
            self.notification_type = notification_type
        else:
            raise AttributeError(f"Notification type not in list of valid notification types: {notification_type}")

class SlackInfoNotification(Notification):
    """Represents a slack notification and specific format."""

    def __init__(self, header=None, message=None):
        super().__init__(notification_type='INFO')
        if header is not None:
            self.header = header
        else:
            self.header = None

        if message is not None:
            self.message = message
        else: 
            self.message = None

    def _generate_blocks(self):
        """
        Generates a valid Slack block kit render from information passed into this class.
        """
        self._header = SlackHeaderBlock(otext=f"{self.header}")

        self._section_1 = SlackSectionBlock(otext=self.message)
        self._block_kit = SlackBlockKit(header=self._header, sections=[self._section_1])         


    def render(self):
        self._generate_blocks()
        return self._block_kit.render_json()


class SlackWarningExpiryNotification(Notification):
    """Represents a slack notification and specific format."""

    def __init__(self, host=None, expiry_date=None, days_remaining=None, serial=None, issuer=None):
        super().__init__(notification_type='WARNING')

        if len(host) == 0 or host is None:
            raise AttributeError("Host information cannot be None or empty.")
        else:
            self.host = host

        if expiry_date is None:
            raise AttributeError("Expiry information cannot be None or empty.")
        else:
            self.expiry_date = expiry_date
        
        if days_remaining is None:
            raise AttributeError("Remaining days information cannot be None or empty.")
        else:
            self.days_remaining = days_remaining

        if serial is None:
            raise AttributeError("Certificate Serial number cannot be None or empty.")
        else:
            self.serial = serial

        if len(issuer) == 0 or issuer is None:
            raise AttributeError("Certificate Issuer cannot be None or empty.")
        else:
            self.issuer = issuer
    
    # I'm going to hell for this :/
    # This definately requires refactoring..
    def _generate_blocks(self):
        """
        Generates a valid Slack block kit render from information passed into this class.
        """
        self._header = SlackHeaderBlock(otext=f"{self.notification_type} - {self.days_remaining} Day Expiry Warning: {self.host}")
        self._field_1 = SlackTextBlock(otype="mrkdwn", otext=f"*Domain*\n{self.host}")        
        self._field_2 = SlackTextBlock(otype="mrkdwn", otext=f"*Serial Number*\n{self.serial}")
        self._field_3 = SlackTextBlock(otype="mrkdwn", otext=f"*Certificate Issuer*\n{self.issuer}")
        self._field_4 = SlackTextBlock(otype="mrkdwn", otext=f"*Remaining Days On Certificate*\n{self.days_remaining}")
        self._field_5 = SlackTextBlock(otype="mrkdwn", otext=f"*Expiry Date*\n{self.expiry_date}")
        self._section_1 = SlackSectionBlock(fields=[self._field_1, self._field_2])
        self._section_2 = SlackSectionBlock(fields=[self._field_3, self._field_4])
        self._section_3 = SlackSectionBlock(fields=[self._field_5])
        self._block_kit = SlackBlockKit(header=self._header, sections=[self._section_1, self._section_2, self._section_3])

    def render(self):
        self._generate_blocks()
        return self._block_kit.render_json()


class SlackCriticalExpiryNotification(Notification):
    """Represents a slack notification and specific format."""

    def __init__(self, host=None, expiry_date=None, days_remaining=None, serial=None, issuer=None):
        super().__init__(notification_type='CRITICAL')

        if len(host) == 0 or host is None:
            raise AttributeError("Host information cannot be None or empty.")
        else:
            self.host = host

        if expiry_date is None:
            raise AttributeError("Expiry information cannot be None or empty.")
        else:
            self.expiry_date = expiry_date
        
        if days_remaining is None:
            raise AttributeError("Remaining days information cannot be None or empty.")
        else:
            self.days_remaining = days_remaining

        if serial is None:
            raise AttributeError("Certificate Serial number cannot be None or empty.")
        else:
            self.serial = serial

        if len(issuer) == 0 or issuer is None:
            raise AttributeError("Certificate Issuer cannot be None or empty.")
        else:
            self.issuer = issuer
    
    # I'm going to hell for this :/
    # This definately requires refactoring..
    def _generate_blocks(self):
        """
        Generates a valid Slack block kit render from information passed into this class.
        """
        self._header = SlackHeaderBlock(otext=f"{self.notification_type} - {self.days_remaining} Day Expiry Warning: {self.host}")
        self._field_1 = SlackTextBlock(otype="mrkdwn", otext=f"*Domain*\n{self.host}")        
        self._field_2 = SlackTextBlock(otype="mrkdwn", otext=f"*Serial Number*\n{self.serial}")
        self._field_3 = SlackTextBlock(otype="mrkdwn", otext=f"*Certificate Issuer*\n{self.issuer}")
        self._field_4 = SlackTextBlock(otype="mrkdwn", otext=f"*Remaining Days On Certificate*\n{self.days_remaining}")
        self._field_5 = SlackTextBlock(otype="mrkdwn", otext=f"*Expiry Date*\n{self.expiry_date}")
        self._section_1 = SlackSectionBlock(fields=[self._field_1, self._field_2])
        self._section_2 = SlackSectionBlock(fields=[self._field_3, self._field_4])
        self._section_3 = SlackSectionBlock(fields=[self._field_5])
        self._block_kit = SlackBlockKit(header=self._header, sections=[self._section_1, self._section_2, self._section_3])

    def render(self):
        self._generate_blocks()
        return self._block_kit.render_json()


class Notifier:
    """This is the base class for the notification object that will handle submitting notifications
    to various vendors."""

    __DATABSE_DIRECTORY = "/var/lib/cert_checker"
    __DATABASE_FILE_NAME = "db.sqlite"
    __CONFIG_DIRECTORY = "/etc/cert_checker"
    __NOTIFICATIONS_CONFIG_FILE = "notifications.yaml"

    def __init__(self, override_config_location=None):
        if override_config_location is None:
            self.config_path = f"{self.__CONFIG_DIRECTORY}/{self.__NOTIFICATIONS_CONFIG_FILE}" 
        else:
            self.config_path = override_config_location
        self.db_path = f"{self.__DATABSE_DIRECTORY}/{self.__DATABASE_FILE_NAME}"
        self.config = config.load_config_file(self.config_path)

    @abstractmethod
    def send_notification(self):
        pass


class SlackWebHook(Notifier):
    """This represents the notification"""

    def __init__(self):
        super().__init__()
        self.logger = logging.getLogger("Slack Web Hook")        
        self._slack_webhook_url = self.config['notifications']['slack']['WebHookURL']
        self.webhook = WebhookClient(url=self._slack_webhook_url)
 
    def send_notification(self, notification: Notification):
        """
        Sends the notification to Slack.
        """
        self.logger.info(f"Sending Slack Notification")
        self.webhook.send(blocks=notification.render())
