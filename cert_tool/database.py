import datetime
from sqlalchemy import Column, Integer, Text, Table, DateTime
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

base = declarative_base()

certs = Table(
    "ssl_certificates",
    base.metadata,
    Column("cert_id", Integer, primary_key=True, nullable=False),
    Column("cert_hostname", Text, nullable=False),
    Column("cert_serial_number", Text, nullable=False)

)
