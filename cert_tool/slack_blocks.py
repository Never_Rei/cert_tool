from typing import List, Dict, Union
import json


class SlackBlockObject:
    """Represents A Slack 'block' object, is a base class."""

    def __init__(self):
        self.otype = None
        self.otext = None


class SlackTextBlock(SlackBlockObject):
    """
    This represents a Slack "text block" object.

    Attributes
    ----------
    otype : str
        Object type for the text block object.
    otext : str
        Object text for the text block object.
    emoji : bool
        Boolean field denoting if Emojis are included.

    Methods
    -------
    render():
        Returns the constructed 'text' object for a slack block
        as a dictionary object. 
    """

    __types = ["mrkdwn", "plain_text"]

    def __init__(self, otype: str, otext: str, emoji: bool = False):
        super().__init__()
        self.__block: Dict = {}
        self.otype = otype
        self.otext = otext
        self.emoji = emoji
        if self.otype not in self.__types:
            raise AttributeError(f"Type: {self.otype} not recognised text field type.")

    def __repr__(self):
        if self.otype == "mrkdwn":
            return f"SlackTextBlock<Type={self.otype}, Text={self.otext}, Emoji={self.emoji}>"
        else:
            return f"SlackTextBlock<Type={self.otype}, Text={self.otext}>"

    def render(self) -> dict:
        self.__block['type'] = self.otype
        self.__block['text'] = self.otext
        if self.otype == "plain_text":
            self.__block['emoji'] = self.emoji           
        return self.__block


# TODO Implement this class
class SlackImageBlock(SlackBlockObject):
    img_url: str = None
    alt_text: str = None

    def __init___(self, url: str, alt_text: str):
        super().__init__()        
        self.otype = "image"
        pass


class SlackSectionBlock(SlackBlockObject):
    """
    This represents a Slack "section block" object.

    Attributes
    ----------
    fields : List[SlackBlockObject]
        List of 'SlackBlockObjects' for use within the section.
    otype : str
        Object type for the text block object.
    otext : str
        Object text for the text block object.

    Methods
    -------
    render():
        Returns the constructed 'section' object for a slack block
        as a dictionary object. 
    """    

    def __init__(self, otext: str = None, fields: List[SlackBlockObject] = None):
        super().__init__()
        self.__section: Dict = {}
        self.fields: List = []        
        self.otype = "section"
        # Validate fields parameter is not empty...
        if fields is None and otext is None:
            raise AttributeError("Fields is required when otext is empty.")

        if fields is not None:
            self.__fields = fields
            for field in self.__fields:
                if isinstance(field, SlackSectionBlock):
                    raise TypeError("Field cannot be a 'SlackSectionBlock'.")
                else:
                    self.fields.append(field.render())
        else:
            self.fields = None

        if otext is not None:
            self.otext = SlackTextBlock(otype="mrkdwn", otext=otext)

    def __repr__(self):
        if self.otext is not None:
            return f"SlackSectionBlock<Type={self.otype}, Text={self.otext}, Fields={self.__fields}>"
        else:     
            return f"SlackSectionBlock<Type={self.otype}, Fields={self.__fields}>"

    def render(self):
        self.__section['type'] = self.otype
        if self.otext is not None:
            self.__section['text'] = self.otext.render()
        if self.fields is not None:
            self.__section['fields'] = self.fields
        return self.__section


class SlackHeaderBlock(SlackBlockObject):

    def __init__(self, otext: str):
        self.__header: Dict = {}        
        self.otype = "header"
        self.otext = otext
        self.__header_text = SlackTextBlock(otype="plain_text", otext=self.otext)    

    def __repr__(self):
        return f"SlackHeaderBlock<Type={self.otype}, Text={self.__header_text}>"        
    
    def render(self):
        self.__header['type'] = self.otype
        self.__header['text'] = self.__header_text.render()       
        return self.__header


class SlackBlockKit:
    __MAX_SECTIONS = 3

    def __init__(self, header: SlackHeaderBlock, sections: List[SlackSectionBlock]):
        self.__header = header
        self.__sections = sections
        self.__blocks: List = []        
        if len(self.__sections) > self.__MAX_SECTIONS:
            raise AttributeError(
                f"Sections is larger than {self.__MAX_SECTIONS} blocks. Sections has "
                f"{len(self.__sections)} of blocks"
            )
        self.__build_blocks()

    def __build_blocks(self):
        self.__blocks.append(self.__header.render())
        for section in self.__sections:
            self.__blocks.append(section.render())
 
    def render(self):
        return self.__blocks

    def render_json(self):
        return json.dumps(self.__blocks)
