import logging
from os import getenv

import yaml

LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

logging.basicConfig(level=logging.INFO,
                    format=LOG_FORMAT)

def load_config_file(config_file) -> dict:
    """
    Loads an arbitrary yaml file and spits out the config as a dictionary.

    Returns: 
        Dictionary Object
    """
    with open(config_file) as file:
        config = yaml.load(file, Loader=yaml.FullLoader)

    return config
