import ssl
from datetime import datetime
from time import sleep
import socket
import logging

from cryptography import x509
from cryptography.hazmat.backends import default_backend
import ipaddress as IP
from fqdn import FQDN
import yaml

import config
from notifications import SlackCriticalExpiryNotification, SlackWarningExpiryNotification, SlackWebHook


class SSLCertificate:
    socket_timeout = 10  # Socket timeout in seconds

    def __init__(self, hostname, port=443, ipaddress=None, connect_to_ip=False):
        self.logger = logging.getLogger("SSL Cert Object")

        if hostname is None or '':
            raise ValueError('Hostname Cannot Be None or Empty String.')

        if connect_to_ip is True and ipaddress is None:
            raise ValueError('IPaddress must be specified if connect_to_ip is True.')

        self.hostname = FQDN(hostname)
        self.port = port
        if ipaddress is not None:
            self.ipaddress = IP.ip_address(ipaddress)
        self.connect_to_ip = connect_to_ip

        self.cert = self.get_cert()

    def get_cert(self):
        """
        Connects and retrieves SSL certificate from peer.

        Returns: 
        x509 object
        """
        context = ssl.create_default_context()
        if self.connect_to_ip is True:
            connection = socket.create_connection((self.ipaddress, self.port))
        else:
            self.logger.info(f"Openning HTTPs Connection to {self.hostname.relative}")
            connection = socket.create_connection((self.hostname.relative, self.port))
        sock = context.wrap_socket(connection, server_hostname=self.hostname.relative)
        sock.settimeout(self.socket_timeout)
        try:
            self.logger.info(f"Getting peer certificate for {self.hostname.relative}")
            der_cert = sock.getpeercert(True)
            # pem_cert = ssl.DER_cert_to_PEM_cert(der_cert)
        finally:
            sock.close()
        # return OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, pem_cert)
        return x509.load_der_x509_certificate(der_cert, default_backend())

    @staticmethod
    def issuer(cert) -> str:
        """
        Returns nice certificate issuer.

        Returns: 
        Certificate Issuer
        """
        return cert.issuer.get_attributes_for_oid(x509.oid.NameOID.COMMON_NAME)[0].value

    @staticmethod
    def serial_number(cert) -> str:
        """
        Returns certificate serial number.

        Returns: 
            Certificate serial number
        """
        return cert.serial_number

    @staticmethod
    def sans(cert) -> list:
        """
        Returns certificate subject alternative names.

        Returns: 
            Certificate SANS
        """
        san = cert.extensions.get_extension_for_class(x509.SubjectAlternativeName)
        return san.value.get_values_for_type(x509.DNSName)

    @staticmethod
    def common_name(cert) -> str:
        """
        Returns certificate common name.

        Returns: 
            Certificate Common Name
        """
        return cert.subject.get_attributes_for_oid(x509.oid.NameOID.COMMON_NAME)[0].value

    @staticmethod
    def valid_dates(cert):
        """
        Returns a set of valid dates for checking certificate expiry.

        Returns: 
            Dictionary of datetime objects
        """
        return dict(valid_from=cert.not_valid_before,
                    valid_until=cert.not_valid_after)


class Checker:

    __DATABSE_DIRECTORY = "/var/lib/cert_checker"
    __DATABASE_FILE_NAME = "db.sqlite"
    __CONFIG_DIRECTORY = "/etc/cert_checker"
    __CERT_CHECK_CONFIG_FILE = "config.yaml"
    __MIN_CHECK_INTERVAL = 21600

    def __init__(self, 
                override_config_location=None, 
                check_interval_override=None):
        self.logger = logging.getLogger("Cert Checker")
        if override_config_location is None:
            self.config_path = f"{self.__CONFIG_DIRECTORY}/{self.__CERT_CHECK_CONFIG_FILE}" 
        else:
            self.config_path = override_config_location
        self.db_path = f"{self.__DATABSE_DIRECTORY}/{self.__DATABASE_FILE_NAME}"
        if check_interval_override is not None:
            self.__MIN_CHECK_INTERVAL = check_interval_override
        self.running = False
        self.config = config.load_config_file(self.config_path)
        self.webhook = SlackWebHook()

    def load_config(self) -> dict:
        """
        Loads yaml config file.

        Returns: 
            Dictionary object
        """
        with open(self.config_path) as file:
            config = yaml.load(file, Loader=yaml.FullLoader)

        return config

    @staticmethod
    def check_expiry(cert) -> dict:
        """
        Checks validity of certificate

        Returns: 
            dictionary object
        """
        exp = dict(cn=cert.common_name(cert.cert),
                   expired=False,
                   expiry_date=None)

        remaining = cert.cert.not_valid_after - datetime.now()

        if remaining.days > 0:
            exp['expired'] = False
            exp['expiry_date'] = cert.cert.not_valid_after
            exp['remaining'] = remaining.days
        else:
            exp['expired'] = True

        return exp

    @staticmethod
    def get_expiry_state(days_remaining, expiry_config) -> str:
        """ 
        This function checks the days remaining until expiry against
        the hosts expiry notification configuration.
        """
        warning_threshold = expiry_config['warning']['threshold']
        critical_threshold = expiry_config['critical']['threshold']
        print(warning_threshold, critical_threshold)
        if days_remaining <= warning_threshold and days_remaining > critical_threshold:
            return 'warning'
        elif days_remaining < warning_threshold and days_remaining <= critical_threshold:
            return 'critical'
        else:
            return 'ok'

    def store_cert(self):
        pass

    @staticmethod
    def __calculate_interval_in_hours__(interval) -> int:
        return interval / 60 / 60

    def run(self) -> None:
        self.running = True
        while self.running is True:
            for site in self.config['sites']:
                self.logger.info(f"Running check against: {site['host']}")
                sslcert = SSLCertificate(hostname=site['host'])
                cert_details = self.check_expiry(sslcert)
                if cert_details['expired'] is False:
                    self.logger.info(f"Certificate has not yet expired: {cert_details['cn']}, "
                                     f"has {cert_details['remaining']} days left.")
                    if cert_details['remaining'] <= site['expiry_config']['critical']['threshold']:
                        self.logger.warning(f"Sending critical expiry notification for {site['host']}")
                        critical_notification = SlackCriticalExpiryNotification(
                            host=site['host'], 
                            expiry_date=cert_details['expiry_date'], 
                            days_remaining=cert_details['remaining'], 
                            serial=sslcert.serial_number(sslcert.cert), 
                            issuer=sslcert.issuer(sslcert.cert))
                        self.webhook.send_notification(notification=critical_notification)                                        
                    elif cert_details['remaining'] <= site['expiry_config']['warning']['threshold']:
                        self.logger.info(f"Sending warning expiry notification for {site['host']}")
                        warning_notification = SlackWarningExpiryNotification(
                            host=site['host'], 
                            expiry_date=cert_details['expiry_date'], 
                            days_remaining=cert_details['remaining'], 
                            serial=sslcert.serial_number(sslcert.cert), 
                            issuer=sslcert.issuer(sslcert.cert))
                        self.webhook.send_notification(notification=warning_notification)                                                                
                else:
                    self.logger.warning(f"Certificate has expired: {cert_details['cn']}")

            self.logger.info(f"Sleeping for {self.__calculate_interval_in_hours__(self.__MIN_CHECK_INTERVAL)} hours")
            sleep(self.__MIN_CHECK_INTERVAL)


# if __name__ == "__main__":
#     # app = Checker(override_config_location='/home/jwashington/Documents/projects/cert_tool/certificate_tool/tests/test_config.yaml')
#     app = Checker()
#     app.run()