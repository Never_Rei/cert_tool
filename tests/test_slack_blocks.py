import unittest
from cert_tool import SlackBlockObject, SlackTextBlock, SlackSectionBlock, SlackHeaderBlock, SlackBlockKit


class TestSlackBlockObject(unittest.TestCase):

    def test_init(self):
        block = SlackBlockObject()
        self.assertIsInstance(block, SlackBlockObject)


test_text = "test_text"
test_text_mrkdwn = {"type": "mrkdwn", "text": test_text}
test_text_plain_text = {"type": "plain_text", "text": test_text, "emoji": False}
test_text_plain_text_with_emoji = {"type": "plain_text", "text": test_text, "emoji": True}


class TestSlackTextBlock(unittest.TestCase):
    
    def test_init(self):
        text_block = SlackTextBlock(otype="mrkdwn", otext=test_text)
        self.assertIsInstance(text_block, SlackTextBlock)

    def test_mrkdwn_instance(self):
        mrkdwn_block = SlackTextBlock(otype="mrkdwn", otext=test_text)
        self.assertEqual(mrkdwn_block.otype, "mrkdwn")
        self.assertEqual(mrkdwn_block.otext, test_text)
        self.assertEqual(mrkdwn_block.emoji, False)
    
    def test_plain_text_instance_emoji_false(self):
        plain_text = SlackTextBlock(otype="plain_text", otext=test_text)
        self.assertEqual(plain_text.otype, "plain_text")
        self.assertEqual(plain_text.otext, test_text)
        self.assertEqual(plain_text.emoji, False)
    
    def test_plain_text_instance_emoji_true(self):
        plain_text2 = SlackTextBlock(otype="plain_text", otext=test_text, emoji=True)
        self.assertEqual(plain_text2.otype, "plain_text")
        self.assertEqual(plain_text2.otext, test_text)
        self.assertEqual(plain_text2.emoji, True)

    def test_mrkdwn_instance_render(self):
        mrkdwn_block = SlackTextBlock(otype="mrkdwn", otext=test_text)
        self.assertEqual(mrkdwn_block.render(), test_text_mrkdwn)

    def test_plain_text_instance_emoji_false_render(self):
        plain_text = SlackTextBlock(otype="plain_text", otext=test_text)
        self.assertEqual(plain_text.render(), test_text_plain_text)

    def test_plain_text_instance_emoji_true_render(self):
        plain_text2 = SlackTextBlock(otype="plain_text", otext=test_text, emoji=True)
        self.assertEqual(plain_text2.render(), test_text_plain_text_with_emoji)

    def test_raise_AttributeError(self):
        self.assertRaises(AttributeError, SlackTextBlock, "unknown", test_text)


test_header_block = {'type': 'header', 'text': {'text': test_text, 'type': 'plain_text', 'emoji': False}}


class TestSlackHeaderBlock(unittest.TestCase):
    def test_init(self):
        header_block = SlackHeaderBlock(otext=test_text)
        self.assertIsNotNone(header_block)
        self.assertIsInstance(header_block, SlackHeaderBlock)

    def test_instantiation(self):
        header_block = SlackHeaderBlock(otext=test_text)
        self.assertEqual(header_block.otext, test_text)
        self.assertEqual(header_block.otype, "header")

    def test_header_render(self):
        header_block = SlackHeaderBlock(otext=test_text)
        self.assertEqual(header_block.render(), test_header_block)        


test_section_block = {'type': 'section', 'fields': [test_text_mrkdwn]}


class TestSlackSectionBlock(unittest.TestCase):
    def test_init(self):
        text_block = SlackTextBlock(otype="mrkdwn", otext=test_text)
        section_block = SlackSectionBlock(fields=[text_block])
        self.assertIsNotNone(section_block)
        self.assertIsInstance(section_block, SlackSectionBlock)

    def test_instantiation(self):
        text_block = SlackTextBlock(otype="mrkdwn", otext=test_text)
        section_block = SlackSectionBlock(fields=[text_block])
        self.assertIsNotNone(section_block.fields)
        self.assertEqual(section_block.fields[0], test_text_mrkdwn)

    def test_raise_TypeError_section_in_fields(self):
        text_block = SlackTextBlock(otype="mrkdwn", otext=test_text)
        section = SlackSectionBlock(otext=test_text, fields=[text_block])   
        self.assertRaises(TypeError, SlackSectionBlock, text_block, [section])

    def test_raise_AttributeError_required_fields(self):
        self.assertRaises(AttributeError, SlackSectionBlock)

    def test_section_render(self):
        text_block = SlackTextBlock(otype="mrkdwn", otext=test_text)
        section_block = SlackSectionBlock(fields=[text_block])        
        self.assertEqual(section_block.render(), test_section_block)


class TestSlackBlockKit(unittest.TestCase):
    def test_init(self):
        text_block = SlackTextBlock(otype="mrkdwn", otext=test_text)
        header_block = SlackHeaderBlock(otext=test_text)
        section_block = SlackSectionBlock(fields=[text_block])
        blocks = SlackBlockKit(header=header_block, sections=[section_block])
        self.assertIsNotNone(blocks)
    
    def test_instantiation(self):
        text_block = SlackTextBlock(otype="mrkdwn", otext=test_text)
        header_block = SlackHeaderBlock(otext=test_text)
        section_block = SlackSectionBlock(fields=[text_block])
        blocks = SlackBlockKit(header=header_block, sections=[section_block])
        self.assertIsNotNone(blocks)        
    
    def test_raise_AttributeError_too_many_sections(self):
        text_block = SlackTextBlock(otype="mrkdwn", otext=test_text)
        header_block = SlackHeaderBlock(otext=test_text)
        section_block = SlackSectionBlock(fields=[text_block])
        blocks = SlackBlockKit(header=header_block, sections=[section_block])
        self.assertRaises(AttributeError, SlackBlockKit, header_block, [section_block, section_block, section_block, section_block])
